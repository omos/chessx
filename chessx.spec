Name:           chessx
Version:        1.5.6
Release:        3%{?dist}
Summary:        Chess Database and PGN viewer

License:        GPLv2
URL:            https://sourceforge.net/projects/chessx/
Source0:        https://downloads.sourceforge.net/%{name}/%{name}-%{version}.tgz

Patch1:         001-qmake-install-support-for-linux-bsd.patch
Patch2:         002-add-metainfo-file.patch
Patch3:         003-fix-icons-installation.patch

# Requires Qt >= 5.7 as per INSTALL
%global min_qt_version 5.7.0

BuildRequires:  gcc-c++
BuildRequires:  qt5-qtbase-devel >= %{min_qt_version}
BuildRequires:  qt5-linguist >= %{min_qt_version}
BuildRequires:  pkgconfig(Qt5Svg) >= %{min_qt_version}
BuildRequires:  pkgconfig(Qt5Multimedia) >= %{min_qt_version}
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib

# Bundles part of scid 1.0 (https://sourceforge.net/projects/scid/)
Provides:       bundled(scid) = 1.0
# Currently bundles quazip
# TODO: Try to patch it to link against system quazip
Provides:       bundled(quazip)

%description
ChessX is a free and open source chess database application for Linux, Mac OS X
and Windows.


%prep
%autosetup -p1 -n %{name}-%{version}


%build
%qmake_qt5 -r
%make_build


%install
%make_install INSTALL_ROOT=%{buildroot}


%check
# Make sure the .metainfo.xml file passes validation
appstream-util validate-relax --nonet \
    %{buildroot}%{_metainfodir}/io.sourceforge.ChessX.metainfo.xml


%files
%license COPYING
%{_bindir}/chessx
%{_datadir}/applications/chessx.desktop
%{_datadir}/icons/hicolor/128x128/apps/chessx.png
%{_datadir}/icons/hicolor/64x64/apps/chessx.png
%{_datadir}/icons/hicolor/32x32/apps/chessx.png
%{_metainfodir}/io.sourceforge.ChessX.metainfo.xml


%changelog
* Wed Jul 07 2021 Ondrej Mosnacek <omosnace@redhat.com> - 1.5.6-3
- Install metadata and icons from upstream

* Sun Jun 13 2021 Ondrej Mosnacek <omosnace@redhat.com> - 1.5.6-2
- Add a desktop file and icon

* Sun Jun 06 2021 Ondrej Mosnacek <omosnace@redhat.com> - 1.5.6-1
- Update to version 1.5.6

* Sun Apr 19 2020 Ondrej Mosnacek <omosnace@redhat.com> - 1.5.0-1
- Initial version
